﻿namespace TSMap
{

    public class OverlayContainer
    {

        public short X
        {
            get;
            set;
        }
        public short Y
        {
            get;
            set;
        }
        public int Index
        {
            get;
            set;
        }
        public int Value
        {
            get;
            set;
        }
        public OverlayContainer(short x = 0, short y = 0, int index = 0, int value = 0)
        {
            X = x;
            Y = y;
            Index = index;
            Value = value;
        }
    }
}
