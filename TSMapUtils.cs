﻿/*
 * Copyright 2017 by Starkku
 * TSMap related modifications by PTapioK
 * This file is part of TSMapGenerator, which is free software. It is made
 * available to you under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version. For more
 * information, see COPYING.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CNCMaps.FileFormats.Encodings;
using CNCMaps.FileFormats.VirtualFileSystem;
using StarkkuUtils.FileTypes;
using StarkkuUtils.Utilities;
using System.Text.RegularExpressions;
using System.Globalization;

namespace TSMap
{
    public class TSMap
    {

        // Tool initialized true/false.
        public bool Initialized
        {
            get;
            set;
        }

        // Map file altered true/false.
        public bool Altered
        {
            get;
            set;
        }

        private string FileInput;

        private INIFile MapConfig;                                                                // Map file.
        private string MapTheater = null;                                                         // Map theater data.
        public int Map_Width;
        public int Map_Height;
        private int Map_FullWidth;
        private int Map_FullHeight;
        private List<MapTileContainer> IsoMapPack5 = new List<MapTileContainer>();                // Map tile data.
        private byte[] OverlayPack = null;                                                        // Map overlay ID data.
        private byte[] OverlayDataPack = null;                                                    // Map overlay frame data.
        private OverlayContainer[,] overlay = null;
        private MapTileContainer[,] tiles = null;
        private MapTileContainer[,] tiles_cart = null;
        private WaypointContainer[,] waypoints = null;
        private TerrainObjectContainer[,] TerrainObjects = null;

        // Map modify options.
        private bool UseMapOptimize = false;
        private bool UseMapCompress = false;

        /// <summary>
        /// Initializes a new instance of TSMap.
        /// </summary>
        /// <param name="inputFile">Input file name.</param>
        /// <param name="outputFile">Output file name.</param>
        /// <param name="fileConfig">Conversion profile file name.</param>
        /// <param name="listTheaterData">If set, it is assumed that this instance of TSMap is initialized for listing theater data rather than processing maps.</param>
        public TSMap(string inputFile)
        {
            Initialized = false;
            Altered = false;
            FileInput = inputFile;

            if (!String.IsNullOrEmpty(FileInput))
            {
                Logger.Info("Reading map file '" + inputFile + "'.");
                MapConfig = new INIFile(inputFile);
                if (!MapConfig.Initialized)
                {
                    Initialized = false;
                    return;
                }
                string[] size = MapConfig.GetKey("Map", "Size", "").Split(',');
                Map_FullWidth = int.Parse(size[2]);
                Map_FullHeight = int.Parse(size[3]);
                Initialized = ParseMapPack();
                ParseOverlayPack();
                MapTheater = MapConfig.GetKey("Map", "Theater", null);
                if (MapTheater != null) MapTheater = MapTheater.ToUpper();
                ParseWaypoints();
                ParseTerrainObjects();
            }
            Initialized = true;
        }

        /// <summary>
        /// Saves the map file.
        /// </summary>
        public void Save(string fileName)
        {
            MapConfig.SetKey("Map", "Size", "0,0," + Map_Width + "," + Map_Height);
            MapConfig.SetKey("Map", "LocalSize", "2,4," + (Map_Width - 4) + "," + (Map_Height - 6));
            if (UseMapOptimize)
            {
                Logger.Info("ApplyMapOptimization set: Saved map will have map section order optimizations applied.");
                MapConfig.MoveSectionToFirst("Basic");
                MapConfig.MoveSectionToFirst("MultiplayerDialogSettings");
                MapConfig.MoveSectionToLast("Digest");
            }
            if (UseMapCompress)
                Logger.Info("ApplyMapCompress set: Saved map will have no unnecessary whitespaces.");
            MapConfig.Save(fileName, !UseMapCompress);
        }
                
        /// <summary>
        /// Parses IsoMapPack5 section of the map file.
        /// </summary>
        /// <returns>True if success, otherwise false.</returns>
        private bool ParseMapPack()
        {
            Logger.Info("Parsing IsoMapPack5.");
            string data = "";
            string[] tmp = MapConfig.GetValues("IsoMapPack5");
            if (tmp == null || tmp.Length < 1) return false;
            data = String.Join("", tmp);
            int cells;
            byte[] isoMapPack;
            try
            {
                string size = MapConfig.GetKey("Map", "Size", "");
                string[] st = size.Split(',');
                Map_Width = Convert.ToInt16(st[2]);
                Map_Height = Convert.ToInt16(st[3]);
                byte[] lzoData = Convert.FromBase64String(data);
                byte[] test = lzoData;
                cells = (Map_Width * 2 - 1) * Map_Height;
                int lzoPackSize = cells * 11 + 4;
                isoMapPack = new byte[lzoPackSize];
                // Fill up and filter later
                int j = 0;
                for (int i = 0; i < cells; i++)
                {
                    isoMapPack[j] = 0x88;
                    isoMapPack[j + 1] = 0x40;
                    isoMapPack[j + 2] = 0x88;
                    isoMapPack[j + 3] = 0x40;
                    j += 11;
                }
                uint total_decompress_size = Format5.DecodeInto(lzoData, isoMapPack);
            }
            catch (Exception)
            {
                return false;
            }
            MemoryFile mf = new MemoryFile(isoMapPack);
            tiles = new MapTileContainer[Map_Width, (Map_Height * 2) + 1];
            tiles_cart = new MapTileContainer[Map_Width * 2, Map_Height * 2];
            for (int i = 0; i < cells; i++)
            {
                ushort x = mf.ReadUInt16();
                ushort y = mf.ReadUInt16();
                int tileNum = mf.ReadInt32();
                byte subTile = mf.ReadByte();
                byte level = mf.ReadByte();
                byte iceGrowth = mf.ReadByte();
                int dx = x - y + Map_FullWidth - 1;
                int dy = x + y - Map_FullWidth - 1;
                if (x > 0 && y > 0 && x <= 16384 && y <= 16384)
                {
                    IsoMapPack5.Add(new MapTileContainer((short)x, (short)y, tileNum, subTile, level, iceGrowth));
                }
                if (dx >= 0 && dx < 2 * Map_Width &&
                    dy >= 0 && dy < 2 * Map_Height)
                {
                    tiles_cart[x, y] = new MapTileContainer((short)x, (short)y, tileNum, subTile, level, iceGrowth);
                    tiles[(ushort)dx / 2, 2 * ((ushort)dy / 2) + dx % 2] = new MapTileContainer((short)x, (short)y, tileNum, subTile, level, iceGrowth);
                }
            }
            return true;
        }

        /// <summary>
        /// Parses Overlay(Data)Pack section(s) of the map file.
        /// </summary>
        private void ParseOverlayPack()
        {
            Logger.Info("Parsing OverlayPack.");
            string[] values = MapConfig.GetValues("OverlayPack");
            if (values == null || values.Length < 1) return;
            byte[] format80Data = Convert.FromBase64String(String.Join("", values));
            var overlaypack = new byte[1 << 18];
            Format5.DecodeInto(format80Data, overlaypack, 80);

            Logger.Info("Parsing OverlayDataPack.");
            values = MapConfig.GetValues("OverlayDataPack");
            if (values == null || values.Length < 1) return;
            format80Data = Convert.FromBase64String(String.Join("", values));
            var overlaydatapack = new byte[1 << 18];
            Format5.DecodeInto(format80Data, overlaydatapack, 80);

            OverlayPack = overlaypack;
            OverlayDataPack = overlaydatapack;

            overlay = new OverlayContainer[Map_FullWidth * 2, Map_FullHeight * 2];
            for (int y = 0; y < 2 * Map_FullHeight; y++)
            {
                for (int x = Map_FullWidth * 2 - 1; x >= 0; x--)
                {
                    var t = tiles_cart[x, y];
                    if (t == null) continue;
                    int idx = t.X + 512 * t.Y;
                    byte overlay_id = OverlayPack[idx];
                    if (overlay_id != 0xFF)
                    {
                        byte overlay_value = OverlayDataPack[idx];
                        var ovl = new OverlayContainer((short)x, (short)y, overlay_id, overlay_value);
                        overlay[x,y] = ovl;
                    }
                }
            }

        }

        private void ParseWaypoints()
        {
            Logger.Info("Parsing Waypoints section");
            waypoints = new WaypointContainer[Map_FullWidth * 2, Map_FullHeight * 2];
            KeyValuePair<string, string>[] items = MapConfig.GetKeyValuePairs("Waypoints");
            foreach (KeyValuePair<string, string> item in items)
            {
                var waypoint = new WaypointContainer();
                waypoint.X = short.Parse(item.Value.Substring(item.Value.Length - 3));
                waypoint.Y = short.Parse(item.Value.Substring(0, item.Value.Length - 3));
                waypoint.Waypoint = short.Parse(item.Key);
                waypoints[waypoint.X, waypoint.Y] = waypoint;
            }
        }

        private void ParseTerrainObjects()
        {
            Logger.Info("Parsing Terrain section");
            TerrainObjects = new TerrainObjectContainer[Map_FullWidth * 2, Map_FullHeight * 2];
            KeyValuePair<string, string>[] items = MapConfig.GetKeyValuePairs("Terrain");
            foreach (KeyValuePair<string, string> item in items)
            {
                var terrain = new TerrainObjectContainer();
                terrain.X = short.Parse(item.Key.Substring(item.Key.Length - 3));
                terrain.Y = short.Parse(item.Key.Substring(0, item.Key.Length - 3));
                terrain.Object = item.Value;
                TerrainObjects[terrain.X, terrain.Y] = terrain;
            }
        }

        public void TerrainToSection(string[,] tileSet, uint width, uint height)
        {
            Logger.Info("Writing terrain to section");
            MapConfig.RemoveSection("Terrain");
            MapConfig.AddSection("Terrain");
            for (var X = 0; X < width; X++)
            {
                for (var Y = 0; Y < height; Y++)
                {
                    if (X > 0 && Y > 0 && X <= 16384 && Y <= 16384)
                    {
                        string t = tileSet[X, Y];

                        if (!String.IsNullOrEmpty(t))
                        {
                            string TileStr = t.ToString();
                            if (TileStr.Split('|').Length > 1)
                            {
                                string terrain = TileStr.Split('|')[1];
                                if (terrain != null)
                                {
                                    MapConfig.SetKey("Terrain", "" + (Y * 1000 + X), "" + terrain);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void WaypointsToSection(string[,] tileSet, uint width, uint height)
        {
            Logger.Info("Writing waypoints to section");
            MapConfig.RemoveSection("Waypoints");
            MapConfig.AddSection("Waypoints");
            for (var X = 0; X < width; X++)
            {
                for (var Y = 0; Y < height; Y++)
                {
                    if (X > 0 && Y > 0 && X <= 16384 && Y <= 16384)
                    {
                        string t = tileSet[X, Y];

                        if (!String.IsNullOrEmpty(t))
                        {
                            string TileStr = t.ToString();
                            if (TileStr.Split(';').Length > 1)
                            {
                                int waypoint = int.Parse(TileStr.Split(';')[1].Split('|')[0]);
                                if (waypoint != -1)
                                {
                                    MapConfig.SetKey("Waypoints", "" + waypoint, "" + (Y * 1000 + X));
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Convert tilearray to overlaypacks
        /// </summary>
        public void OverlaySetToOverlayPack(string[,] tileSet, uint width, uint height)
        {
            var overlaypack = new byte[1 << 18];
            var overlaydatapack = new byte[1 << 18];

            for (var X = 0; X < width; X++)
            {
                for (var Y = 0; Y < height; Y++)
                {
                    if (X > 0 && Y > 0 && X <= 16384 && Y <= 16384)
                    {
                        Int32 idx = X + 512 * Y;
                        string t = tileSet[X, Y].Split(';')[0].Split('|')[0];

                        byte overlay_index = 0xFF;
                        byte overlay_data = 0x0;
                        if (!String.IsNullOrEmpty(t))
                        {
                            string TileStr = t.ToString();

                            overlay_index = (byte)Int32.Parse(TileStr.Split('_')[3]);
                            overlay_data = (byte)Int32.Parse(TileStr.Split('_')[4]);
                        }
                        else
                        {
                            overlay_index = 0xFF;
                            overlay_data = 0x0;
                        }

                        overlaypack[idx] = overlay_index;
                        overlaydatapack[idx] = overlay_data;
                    }
                }
            }

            string base64_overlayPack = Convert.ToBase64String(Format5.Encode(overlaypack, 80), Base64FormattingOptions.None);
            string base64_overlayDataPack = Convert.ToBase64String(Format5.Encode(overlaydatapack, 80), Base64FormattingOptions.None);
            OverrideBase64MapSection("OverlayPack", base64_overlayPack);
            OverrideBase64MapSection("OverlayDataPack", base64_overlayDataPack);
            Altered = true;
        }

        /// <summary>
        /// Replaces contents of a base64-encoded section of map file.
        /// </summary>
        /// <param name="sectionName">Name of the section to replace.</param>
        /// <param name="data">Contents to replace the existing contents with.</param>
        private void OverrideBase64MapSection(string sectionName, string data)
        {
            int lx = 70;
            List<string> lines = new List<string>();
            for (int x = 0; x < data.Length; x += lx)
            {
                lines.Add(data.Substring(x, Math.Min(lx, data.Length - x)));
            }
            MapConfig.ReplaceSectionValues(sectionName, lines);
        }
        
        /// <summary>
        /// Converts tileset data into compressed IsoMapPack5 format.
        /// </summary>
        public void TileSetToMapPack(string[,] tileSet, uint width, uint height, uint count)
        {
            byte[] isoMapPack = new byte[count * 11 + 4];
            int i = 0;

            for (var X = 0; X < width; X++)
            {
                for (var Y = 0; Y < height; Y++)
                {
                    if (X > 0 && Y > 0 && X <= 16384 && Y <= 16384)
                    {
                        string t = tileSet[X, Y];
                        byte[] x = BitConverter.GetBytes(X);
                        byte[] y = BitConverter.GetBytes(Y);
                        if (!String.IsNullOrEmpty(t))
                        {
                            string TileStr = t.ToString().Split(';')[0].Split('|')[0];

                            byte[] tilei = BitConverter.GetBytes(Int32.Parse(TileStr.Split('_')[0]));
                            isoMapPack[i] = x[0];
                            isoMapPack[i + 1] = x[1];
                            isoMapPack[i + 2] = y[0];
                            isoMapPack[i + 3] = y[1];
                            isoMapPack[i + 4] = tilei[0];
                            isoMapPack[i + 5] = tilei[1];
                            isoMapPack[i + 6] = tilei[2];
                            isoMapPack[i + 7] = tilei[3];
                            isoMapPack[i + 8] = (byte)Int32.Parse(TileStr.Split('_')[1]);
                            isoMapPack[i + 9] = (byte)Int32.Parse(TileStr.Split('_')[2]);
                        } else
                        {
                            byte[] tilei = BitConverter.GetBytes(0);
                            isoMapPack[i] = x[0];
                            isoMapPack[i + 1] = x[1];
                            isoMapPack[i + 2] = y[0];
                            isoMapPack[i + 3] = y[1];
                            isoMapPack[i + 4] = tilei[0];
                            isoMapPack[i + 5] = tilei[1];
                            isoMapPack[i + 6] = tilei[2];
                            isoMapPack[i + 7] = tilei[3];
                            isoMapPack[i + 8] = 0;
                            isoMapPack[i + 9] = 0;
                        }
                        isoMapPack[i + 10] = 0;
                        i += 11;
                    }
                }
            }

            byte[] lzo = Format5.Encode(isoMapPack, 5);
            string data = Convert.ToBase64String(lzo, Base64FormattingOptions.None);
            OverrideBase64MapSection("IsoMapPack5", data);
        }
        
        public MapTileContainer[,] GetTiles()
        {
            return tiles_cart;
        }

        public OverlayContainer[,] GetOverlay()
        {
            return overlay;
        }

        public WaypointContainer[,] GetWaypoints()
        {
            return waypoints;
        }

        public TerrainObjectContainer[,] GetTerrain()
        {
            return TerrainObjects;
        }
    }
}
