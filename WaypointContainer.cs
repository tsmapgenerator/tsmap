﻿namespace TSMap
{
    public class WaypointContainer
    {
        public short Waypoint
        {
            get;
            set;
        }
        public short X
        {
            get;
            set;
        }
        public short Y
        {
            get;
            set;
        }
    }
}
