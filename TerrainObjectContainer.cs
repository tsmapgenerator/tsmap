﻿namespace TSMap
{
    public class TerrainObjectContainer
    {
        public string Object
        {
            get;
            set;
        }
        public short X
        {
            get;
            set;
        }
        public short Y
        {
            get;
            set;
        }
    }
}
